<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;
use DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::truncate();
        DB::table('posts')->insert([
            ['title'=>'title 1', 'des'=>'des 1', 'detail'=>'detail 1', 'category'=>0, 'public'=>false, 'data_public'=> '2019-06-28', 'position'=>'[1,2,3]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 2', 'des'=>'des 2', 'detail'=>'detail 2', 'category'=>0, 'public'=>true, 'data_public'=> '2019-06-28', 'position'=>'[1,2]', 'thumbs'=>'thumb.img'],
            
            ['title'=>'title 3', 'des'=>'des 3', 'detail'=>'detail 3', 'category'=>1, 'public'=>false, 'data_public'=> '2020-06-28', 'position'=>'[1]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 4', 'des'=>'des 4', 'detail'=>'detail 4', 'category'=>1, 'public'=>true, 'data_public'=> '2020-06-28', 'position'=>'[1]', 'thumbs'=>'thumb.img'],
            
            ['title'=>'title 5', 'des'=>'des 5', 'detail'=>'detail 5', 'category'=>2, 'public'=>false, 'data_public'=> '2020-06-28', 'position'=>'[1]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 6', 'des'=>'des 6', 'detail'=>'detail 6', 'category'=>2, 'public'=>false, 'data_public'=> '2021-09-01', 'position'=>'[1,2]', 'thumbs'=>'thumb.img'],
            
            ['title'=>'title 7', 'des'=>'des 7', 'detail'=>'detail 7', 'category'=>3, 'public'=>true, 'data_public'=> '2021-09-01', 'position'=>'[1]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 8', 'des'=>'des 8', 'detail'=>'detail 8', 'category'=>3, 'public'=>false, 'data_public'=> '2021-09-01', 'position'=>'[4]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 9', 'des'=>'des 9', 'detail'=>'detail 9', 'category'=>3, 'public'=>false, 'data_public'=> '2021-09-01', 'position'=>'[1]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 10', 'des'=>'des 10', 'detail'=>'detail 10', 'category'=>4, 'public'=>false, 'data_public'=> '2019-06-28', 'position'=>'[4]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 11', 'des'=>'des 11', 'detail'=>'detail 11', 'category'=>5, 'public'=>true, 'data_public'=> '2019-06-28', 'position'=>'[3,4]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 12', 'des'=>'des 12', 'detail'=>'detail 12', 'category'=>5, 'public'=>false, 'data_public'=> '2019-06-28', 'position'=>'[1]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 13', 'des'=>'des 13', 'detail'=>'detail 13', 'category'=>6, 'public'=>true, 'data_public'=> '2019-06-28', 'position'=>'[1]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 14', 'des'=>'des 14', 'detail'=>'detail 14', 'category'=>6, 'public'=>true, 'data_public'=> '2018-12-21', 'position'=>'[4]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 15', 'des'=>'des 15', 'detail'=>'detail 15', 'category'=>7, 'public'=>true, 'data_public'=> '2019-06-28', 'position'=>'[1]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 16', 'des'=>'des 16', 'detail'=>'detail 16', 'category'=>8, 'public'=>false, 'data_public'=> '2018-12-21', 'position'=>'[3]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 17', 'des'=>'des 17', 'detail'=>'detail 17', 'category'=>9, 'public'=>false, 'data_public'=> '2019-06-28', 'position'=>'[1,3,4]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 18', 'des'=>'des 18', 'detail'=>'detail 18', 'category'=>10, 'public'=>true, 'data_public'=> '2018-12-21', 'position'=>'[3]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 19', 'des'=>'des 19', 'detail'=>'detail 19', 'category'=>11, 'public'=>false, 'data_public'=> '2019-06-28', 'position'=>'[1]', 'thumbs'=>'thumb.img'],
            ['title'=>'title 20', 'des'=>'des 20', 'detail'=>'detail 20', 'category'=>12, 'public'=>false, 'data_public'=> '2019-06-28', 'position'=>'[1]', 'thumbs'=>'thumb.img'],
        ]);
    }
}
